const http = require("http");
const uuid = require("uuid");
const path = require("path");
const fs = require("fs");

const PORT = process.env.PORT || 4000;

const server = http.createServer((request, response) => {
  
  if (request.url === "/html") {

    // Return html file data for /html request
    fs.readFile(path.join(__dirname, "htmlFile.html"), "utf-8",(errorWhileReadingHTML, htmlData) => {
        if (errorWhileReadingHTML) {
          console.error(errorWhileReadingHTML);
          response.writeHead(404, { "Content-Type": "text/html" });
          response.write("Error reading html file.");
          response.end();
        }
        else {
          response.writeHead(200, { "Content-Type": "text/html" });
          response.write(htmlData);
          response.end();
        }
    });
    
  }
  else if (request.url === "/json") {

    // Return json file data for /json request
    fs.readFile(path.join(__dirname, "jsonFile.json"), "utf-8",(errorWhileReadingJSON, jsonData) => {
        if (errorWhileReadingJSON) {
          console.error(errorWhileReadingJSON);
          response.writeHead(404, { "Content-Type": "text/html" });
          response.write("Error reading json file.");
          response.end();
        }
        else {
          response.writeHead(200, { "Content-Type": "application/json" });
          response.write(jsonData);
          response.end();
        }
    });
    
  } 
  else if (request.url === "/uuid") {

    // Return random UUIDV4 for /uuid request
    let randomUUID4 = uuid.v4();
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify({ uuid: randomUUID4 }));
    response.end();

  }
  else if (request.url.indexOf("/status/") > -1) {

    // Return response for /status/statusCode request
    const statusCode = request.url.split("/");
    if (statusCode.length === 3) {
      let code = statusCode[statusCode.length - 1];
      if (!isNaN(code) && Object.keys(http.STATUS_CODES).includes(code.toString())) {
        let validCode = parseInt(code);
        response.writeHead(validCode, { "Content-Type": "text/html" });
        response.write(`Status code ${validCode}:- ${http.STATUS_CODES[validCode.toString()]}`);
        response.end();
      }
      else if( code.length == 0) {
        response.writeHead(400, {"Content-Type": "text/html"});
        response.write(`Status code cannot be empty.`);
        response.end();
      }
      else {
        response.writeHead(400, {"Content-Type": "text/html" });
        response.write(code + " is not a valid http status code.");
        response.end();
      }
    }
    else {
      response.writeHead(400, {"Content-Type": "text/html" });
      response.write(statusCode.slice(2).join("/") + " is not a valid http status code.");
      response.end();
    }

  }
  else if (request.url.indexOf("/delay/") > -1) {

    // Return delayed response for /delay/seconds request
    const delayInSeconds = request.url.split("/");
    if (delayInSeconds.length === 3) {
      const seconds = delayInSeconds[delayInSeconds.length - 1];
      if (!isNaN(seconds) && seconds.length > 0) {
        const validSeconds = parseFloat(seconds);
        if (validSeconds >= 0) {
          setTimeout(() => {
            response.writeHead(200, { "Content-Type": "application/json" });
            response.write(JSON.stringify({ "Delay seconds": validSeconds }));
            response.end();
          }, validSeconds * 1000);
        }
        else {
          response.writeHead(400, { "Content-Type": "text/html" });
          response.write(`Delay time cannot be a negative value.`);
          response.end();
        }
      }
      else if (seconds.length === 0) {
        response.writeHead(400);
        response.write(`Delay seconds cannot be empty.`);
        response.end();
      }
      else {
        response.writeHead(400,);
        response.write(`${seconds} is not a valid delay time.`);
        response.end();
      }
    }
    else {
      response.writeHead(400, {"Content-Type": "text/html" });
      response.write(delayInSeconds.slice(2).join("/") + " is not a valid delay time.");
      response.end();
    }
  }
  else {

    // Return response for invalid request
    console.error(`Invalid HTTP request`);
    response.writeHead(404, "File not found.");
    response.write(`Error code: ${http.STATUS_CODES[404]}`);
    response.end();

  }

});

server.listen(PORT, () => {
  console.log(`Server initialized on port ${PORT}`);
});
